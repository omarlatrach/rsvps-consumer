package com.thedevopsnerdworld.rsvps.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.support.converter.RecordMessageConverter;
import org.springframework.kafka.support.converter.StringJsonMessageConverter;

@Configuration
public class RsvpConfig {

	@Bean
	public RecordMessageConverter converter() {
		return new StringJsonMessageConverter();
	}

}
