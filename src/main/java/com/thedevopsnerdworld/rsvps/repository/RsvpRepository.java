package com.thedevopsnerdworld.rsvps.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.thedevopsnerdworld.rsvps.model.MeetupRSVP;

public interface RsvpRepository extends MongoRepository<MeetupRSVP, String> {

}
