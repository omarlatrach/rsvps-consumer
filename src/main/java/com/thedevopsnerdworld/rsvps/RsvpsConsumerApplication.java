package com.thedevopsnerdworld.rsvps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RsvpsConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(RsvpsConsumerApplication.class, args);
	}

}
