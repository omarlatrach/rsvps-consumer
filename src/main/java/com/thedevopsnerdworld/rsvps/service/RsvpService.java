package com.thedevopsnerdworld.rsvps.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.thedevopsnerdworld.rsvps.model.MeetupRSVP;
import com.thedevopsnerdworld.rsvps.model.Venue;
import com.thedevopsnerdworld.rsvps.repository.RsvpRepository;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class RsvpService {
	@Autowired
	private RsvpRepository repo;

	@KafkaListener(topics = "meetupTopic", groupId = "group_id")
	public void consume(MeetupRSVP message) throws IOException {
		String city = message.getGroup().getGroup_city();
		Venue venue = message.getVenue();
		String venueName = venue != null ? venue.getVenue_name() : null;
		if (city.contentEquals("Paris") && venueName != null && !venueName.contentEquals("Online event")
				&& !venueName.contentEquals("Online Webinar")) {
			log.info("Saving\n**** Title: {}\n**** Address: {}", message.getEvent().getEvent_name(), venueName);
			repo.save(message);
		}
	}

}
