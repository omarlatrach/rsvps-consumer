package com.thedevopsnerdworld.rsvps.model;

import lombok.Data;

@Data
public class GroupTopic {

	private String urlkey;
	private String topic_name;

}