package com.thedevopsnerdworld.rsvps.model;

import lombok.Data;

@Data
public class Member {

	private Integer member_id;
	private String photo;
	private String member_name;

}